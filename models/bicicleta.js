var Bicicleta = function (id,color,modelo, ubicacion){
		this.id = id;
		this.color = color;
		this.modelo = modelo;
		this.ubicacion= ubicacion;
}

Bicicleta.prototype.toString = function (){
	return 'id: ' + this.id + " color:  "+ this.color;

}
Bicicleta.allBicis = [];
Bicicleta.add = function(aBici){
	Bicicleta.allBicis.push(aBici);

}
Bicicleta.findById = function(aBiciId){
	var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
	if(aBici)
		return aBici;
	else
		throw new Error(`No existe una bicicleta con el id ${aBiciId}`);
}
Bicicleta.removeById = function(aBiciId){
	for (var i=0; i< Bicicleta.allBicis.length; i++){
		if(Bicicleta.allBicis[i].id == aBiciId){
			Bicicleta.allBicis.splice(i,1);
			break;
		}

	}

}
var a= new Bicicleta(1,'rojo','modelo a',[-27.333506719860996, -55.86358631816653]);
var b= new Bicicleta(2,'azul','modelo b',[-27.343506719860996, -55.86458631816653]);
var c= new Bicicleta(3,'amarillo','modelo c',[-27.353506719860996, -55.86558631816653]);

Bicicleta.add(a);
Bicicleta.add(b);
Bicicleta.add(c);
 
module.exports = Bicicleta;